#!/bin/bash
# Copyright Huawei Technologies Co., Ltd. 2022. All rights reserved.

declare -A DEFAULT_CONFIGURE_ARGS=(
    [with-extra-cflags]="-fno-aggressive-loop-optimizations \
                        -fno-gnu-unique \
                        -Wno-unused-parameter \
                        " \
#    [with-extra-ldflags]="-Wl,-z,now,--wrap=memcpy" \
    [enable-unlimited-crypto]=""
#    [enable-jfr]="" \
)

export DEFAULT_MAKE_ARGS="LOG=debug"
export RELEASE_MAKE_TARGETS="product-images legacy-jre-image"
if [[ ${ADD_BUILD_NUMBER} = "false" ]]; then
export TARGET_FILE_TEMPLATE="bisheng-jdk-21.0.REPLACE-linux-x64"
else
export TARGET_FILE_TEMPLATE="bisheng-jdk-21.0.REPLACE-b$BUILD_JDK_BUILD_NUMBER-linux-x64"
fi
export JTREG_TEST_EXCLUDE="compiler/c2/Test8004741.java \
                           runtime/Thread/AsyncExceptionOnMonitorEnter.java \
                           runtime/jni/abstractMethod/TestJNIAbstractMethod.java \
                           runtime/Thread/AsyncExceptionTest.java \
                           runtime/Thread/TestBreakSignalThreadDump.java#default \
                           runtime/Thread/TestBreakSignalThreadDump.java#with_jsig \
                           gc/g1/humongousObjects/objectGraphTest/TestObjectGraphAfterGC.java \
                           gc/shenandoah/compiler/TestLinkToNativeRBP.java \
                           runtime/exceptionMsgs/ArrayIndexOutOfBoundsException/ArrayIndexOutOfBoundsExceptionTest.java#id0 \
                           runtime/exceptionMsgs/ArrayStoreException/ArrayStoreExceptionTest.java \
                           runtime/handshake/HandshakeSuspendExitTest.java \
                           runtime/handshake/HandshakeDirectTest.java \
                           runtime/exceptionMsgs/ArrayIndexOutOfBoundsException/ArrayIndexOutOfBoundsExceptionTest.java#id1 \
                           runtime/handshake/SuspendBlocked.java \
                           runtime/jni/getCreatedJavaVMs/TestGetCreatedJavaVMs.java \
                           runtime/jni/nativeStack/TestNativeStack.java \
                           runtime/jsig/Testjsig.java \
                           runtime/logging/loadLibraryTest/LoadLibraryTest.java \
                           runtime/posixSig/TestPosixSig.java \
                           gc/g1/TestPeriodicCollectionJNI.java \
                           runtime/Thread/StopAtExit.java \
                           runtime/Thread/SuspendAtExit.java \
                           gc/cslocker/TestCSLocker.java \
                           gc/TestJNIWeak/TestJNIWeak.java \
                           runtime/BoolReturn/JNIBooleanTest.java \
                           runtime/BootClassAppendProp/GetBootClassPathAppendProp.java \
                           runtime/BoolReturn/NativeSmallIntCallsTest.java \
                           runtime/cds/serviceability/ReplaceCriticalClasses.java \
                           runtime/cds/serviceability/ReplaceCriticalClassesForSubgraphs.java \
                           runtime/clinit/ClassInitBarrier.java \
                           runtime/DefineClass/NullClassBytesTest.java \
                           runtime/exceptionMsgs/NoClassDefFoundError/NoClassDefFoundErrorTest.java \
                           runtime/handshake/HandshakeTransitionTest.java \
                           runtime/handshake/SystemMembarHandshakeTransitionTest.java \
                           runtime/jni/8025979/UninitializedStrings.java \
                           runtime/jni/activeDestroy/TestActiveDestroy.java \
                           runtime/jni/8033445/DefaultMethods.java \
                           runtime/jni/CalleeSavedRegisters/FPRegs.java \
                           runtime/jni/CallWithJNIWeak/CallWithJNIWeak.java \
                           runtime/jni/atExit/TestAtExit.java \
                           runtime/jni/checked/TestCheckedJniExceptionCheck.java \
                           runtime/jni/checked/TestCheckedReleaseCriticalArray.java \
                           runtime/jni/checked/TestCheckedReleaseArrayElements.java \
                           runtime/jni/FastGetField/FastGetField.java \
                           runtime/jni/daemonDestroy/TestDaemonDestroy.java \
                           runtime/jni/checked/TestPrimitiveArrayCriticalWithBadParam.java \
                           runtime/jni/IsVirtualThread/IsVirtualThread.java#default \
                           runtime/jni/FindClassUtf8/FindClassUtf8.java \
                           runtime/jni/IsVirtualThread/IsVirtualThread.java#no-vmcontinuations \
                           runtime/jni/FindClass/FindClassFromBoot.java \
                           runtime/jni/PrivateInterfaceMethods/PrivateInterfaceMethods.java \
                           runtime/jni/registerNativesWarning/TestRegisterNativesWarning.java \
                           runtime/jni/ReturnJNIWeak/ReturnJNIWeak.java \
                           runtime/jni/terminatedThread/TestTerminatedThread.java \
                           runtime/jni/ToStringInInterfaceTest/ToStringTest.java \
                           runtime/modules/getModuleJNI/GetModule.java \
                           runtime/Monitor/CompleteExit.java \
                           runtime/Nestmates/privateConstructors/TestJNI.java \
                           runtime/Nestmates/privateFields/TestJNI.java \
                           runtime/Nestmates/privateMethods/TestJNI.java \
                           runtime/Nestmates/privateMethods/TestJNIHierarchy.java \
                           runtime/Nestmates/privateStaticFields/TestJNI.java \
                           runtime/Nestmates/privateStaticMethods/TestJNI.java \
                           runtime/SameObject/SameObject.java \
                           runtime/StackGuardPages/TestStackGuardPages.java \
                           runtime/StackGap/TestStackGap.java \
                           runtime/StackGuardPages/TestStackGuardPagesNative.java \
                           runtime/TLS/TestTLS.java \
                           runtime/lockStack/TestStackWalk.java \
                           runtime/vthread/JNIMonitor/JNIMonitor.java"
