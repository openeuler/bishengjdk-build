#!/bin/bash
# Copyright Huawei Technologies Co., Ltd. 2022. All rights reserved.

declare -A DEFAULT_CONFIGURE_ARGS=(
    [with-extra-cflags]="-fno-aggressive-loop-optimizations \
                        -fno-gnu-unique \
                        -Wno-unused-parameter \
                        " \
    [with-extra-ldflags]="-Wl,-z,now,--wrap=memcpy" \
    [enable-unlimited-crypto]=""
    [enable-jfr]="" \
)

export DEFAULT_MAKE_ARGS="LOG=debug"
export RELEASE_MAKE_TARGETS="images"
if [[ ${ADD_BUILD_NUMBER} = "false" ]]; then
export TARGET_FILE_TEMPLATE="bisheng-jdk-8uREPLACE-linux-x64"
else
export TARGET_FILE_TEMPLATE="bisheng-jdk-8uREPLACE-b$BUILD_JDK_BUILD_NUMBER-linux-x64"
fi

export JTREG_TEST_EXCLUDE="runtime/jni/abstractMethod/TestJNIAbstractMethod.java \
                           java/lang/ClassLoader/deadlock/GetResource.java \
                           gc/g1/TestFromCardCacheIndex.java \
                           gc/g1/TestG1NUMATouchRegions.java \
                           com/sun/jdi/RedefineCrossEvent.java \
                           java/util/TimeZone/DefaultTimeZoneTest.java \
                           tools/javac/diags/CheckExamples.java \
                           testlibrary_tests/TestMutuallyExclusivePlatformPredicates.java \
                           compiler/rtm/locking/TestRTMAbortRatio.java \
                           compiler/rtm/locking/TestRTMAbortThreshold.java \
                           compiler/rtm/locking/TestRTMAfterNonRTMDeopt.java \
                           compiler/rtm/locking/TestRTMDeoptOnHighAbortRatio.java \
                           compiler/rtm/locking/TestRTMDeoptOnLowAbortRatio.java \
                           compiler/rtm/locking/TestRTMLockingCalculationDelay.java \
                           compiler/rtm/locking/TestRTMLockingThreshold.java \
                           compiler/rtm/locking/TestRTMRetryCount.java \
                           compiler/rtm/locking/TestRTMSpinLoopCount.java \
                           compiler/rtm/locking/TestRTMTotalCountIncrRate.java \
                           compiler/rtm/locking/TestUseRTMAfterLockInflation.java \
                           compiler/rtm/locking/TestUseRTMDeopt.java \
                           compiler/rtm/locking/TestUseRTMForInflatedLocks.java \
                           compiler/rtm/locking/TestUseRTMForStackLocks.java \
                           compiler/rtm/locking/TestUseRTMXendForLockBusy.java \
			   security/infra/java/security/cert/CertPathValidator/certification/CAInterop.java \
                           security/infra/java/security/cert/CertPathValidator/certification/EmSignRootG2CA.java \
                           security/infra/java/security/cert/CertPathValidator/certification/HaricaCA.java \
                           security/infra/java/security/cert/CertPathValidator/certification/LuxTrustCA.java \
                           security/infra/java/security/cert/CertPathValidator/certification/DigicertCSRootG5.java \
                           security/infra/java/security/cert/CertPathValidator/certification/DTrustCA.java \
                           security/infra/java/security/cert/CertPathValidator/certification/CertignaCA.java"
